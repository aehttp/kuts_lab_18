/* file name: kuts_lab_18_2
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 11.05.2022
*дата останньої зміни 12.05.2022
*лабораторна №18
*варіант : №4
*/
#include <iostream>
#include <Windows.h>
#include <time.h>
using namespace std;
struct p
{
	int e;
	p* next;
};

int fibonacci(int number)
{
    if (number == 0)
        return 0;
    if (number == 1)
        return 1;
    return fibonacci(number-1) + fibonacci(number-2);
}

void print(p* list)//для виводу
{
	while (list)
	{
		cout << list->e << "  "; 
		list = list->next;
	}
}

int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int i=0, j, m, n=0,sumMinandMax,min=50,max=-50;
	p* begin = NULL; // вказівник на голову списку
	p* last = NULL; // вказівник на поточний останній елемент списку
	p* list = NULL; // вказівник на поточний елемент списку
	p* begin1 = NULL; // вказівник на голову списку
	p* last1 = NULL; // вказівник на поточний останній елемент списку
	p* list1 = NULL; // вказівник на поточний елемент списку
	p* begin2 = NULL; // вказівник на голову списку
	p* last2 = NULL; // вказівник на поточний останній елемент списку
	p* list2 = NULL; // вказівник на поточний елемент списку
	cout << "Введіть розмір масиву = ";
	cin >> m;
	while (m>n)
	{
		last1 = new p;
		last1->e=rand()%100-50;
		if (last1->e>max)
			max=last1->e;
		if (last1->e<min)
			min=last1->e;
		
		if (begin1 == NULL)
		{
			last1->next = NULL; 
			begin1 = last1;
		}
		else
		{
			list1->next = last1;
			last1->next = NULL;
		}
		list1 = last1;
		n++;
	}
	cout << "\nВхідний масив a = \n";
	print(begin1);
	list1=begin1;
	int qw=0;
	sumMinandMax=min+max;
	list1=begin1;
	while(list1)
	{
		if(list1->e!=0)
		{
			qw++;
			last2 = new p;
			last2->e=sumMinandMax;
			if (begin2 == NULL)
			{
				last2->next = NULL; 
				begin2 = last2;
			}
			else
			{
				list2->next = last2;
				last2->next = NULL;
			}
			list2 = last2;
		}
		list1=list1->next;
	}
	
	cout << "\nПеретворений масив = \n";
	print(begin2);
	cout <<"\nКількість замін = "<<qw<<"\n";
	cout << "\nВведіть розмір масиву = ";
	cin >> m;
	n=1;
	while (m+1>n)
	{
		last = new p;
		last->e=fibonacci(n);
		if (begin == NULL)
		{
			last->next = NULL; 
			begin = last;
		}
		else
		{
			list->next = last;
			last->next = NULL;
		}
		list = last;
		n++;
	}
	cout << "\nВхідний масив за Фібоначі = \n";
	print(begin);
	while (begin2)
	{
		list2 = begin2;
		begin2 = list2->next;
		delete list2;
	}
	while (begin1)
	{
		list1 = begin1;
		begin1 = list1->next;
		delete list1;
	}
	while (begin)
	{
		list = begin;
		begin = list->next;
		delete list;
	}
	cout << endl;
	system("pause");
	return 0;
}

