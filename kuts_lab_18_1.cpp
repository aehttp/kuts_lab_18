/* file name: kuts_lab_18_1 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 10.05.2022
*дата останньої зміни 10.05.2022
*лабораторна №18
*варіант : №4
*/
#include <iostream>
#include <windows.h>
using namespace std;

struct p
{
int e;
p *next;
};

// процедура виведення елементів списку
void print(p *list)
{
while (list)
{
cout<<list->e<<" ";
list = list->next;
}
}
int main()
{
system("cls");
SetConsoleCP(1251);
SetConsoleOutputCP(1251);
int k, n;
p *begin = NULL; // вказівник на голову списку
p *last = NULL; // вказівник на поточний останній елемент списку
p *list = NULL; // вказівник на поточний елемент списку
cout<<"Введіть кількість елементів = ";
cin>>n;
while (n>0)
{
last = new p;
last->e = rand()%100-50;
if (begin == NULL)
{
last->next = NULL;
begin = last;
}
else
{
last->next = NULL;
list->next = last;
}
list = last;
n--;
}
// виводимо елементи списку
cout <<"\nВхідний масив у вигляді списку = \n";
print(begin);

list = begin;
int max=list->e;
int min=list->e;
while (list)
{
if (list->e>max)
{
max=list->e;
}
if (list->e<min)
{
min=list->e;
}
list = list->next;
}

list = begin;
while (list)
{
if (list->e!=max && list->e!=min)
{
list->e=0;
}
list = list->next;
}
// виводимо елементи списку
cout <<"\nПеретворений масив у вигляді списку = \n";
print(begin);
//звільняємо виділену динамічну пам'ять під список
while (begin)
{
list = begin;
begin = list->next;
delete list;
}
delete begin;
return 0;
}

