/* file name: kuts_lab_18_3
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 12.05.2022
*дата останньої зміни 14.05.2022
*лабораторна №18
*варіант : №4
*/
#include <iostream>
#include <Windows.h>
#include <time.h>
using namespace std;
struct p
{
	int e;
	p* next;
};

int test(p* list1, p* list2)
{
	int q=0, i=0;
	while (list1)
	{
		q+=list1->e;
		list1 = list1->next;
	}
	while (list2)
	{
		i+=list2->e;
		list2 = list2->next;
	}
	if(i>=q)
	return 1;
	else
	return 0;
}

void print(p* list)//для виводу
{
	while (list)
	{
		cout << list->e << "  "; 
		list = list->next;
	}
}

int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int i=0, j, m, n=0;
	p* begin1 = NULL; // вказівник на голову списку
	p* last1 = NULL; // вказівник на поточний останній елемент списку
	p* list1 = NULL; // вказівник на поточний елемент списку
	p* begin2 = NULL; // вказівник на голову списку
	p* last2 = NULL; // вказівник на поточний останній елемент списку
	p* list2 = NULL; // вказівник на поточний елемент списку
	
	cout << "Введіть розмір множин = ";
	cin >> m;
	while (m>n)
	{
		last1 = new p;
		
		last1->e=rand()%10-5;
		/*system("cls");
		printf("Ведіть множину а = ");
		cin >> last1->e;*/
		if (begin1 == NULL)
		{
			last1->next = NULL; 
			begin1 = last1;
		}
		else
		{
			list1->next = last1;
			last1->next = NULL;
		}
		list1 = last1;
		n++;
	}
	n=0;
	while (m>n)
	{
		last2 = new p;
		
		last2->e=rand()%10-5;
		/*system("cls");
		printf("Ведіть множину б = ");
		cin >> last2->e;*/
		
		if (begin2 == NULL)
		{
			last2->next = NULL; 
			begin2 = last2;
		}
		else
		{
			list2->next = last2;
			last2->next = NULL;
		}
		list2 = last2;
		n++;
	}
	cout << "\nВхідний масив у множин а = \n";
	print(begin1);
	cout << "\nВхідний масив у множин b = \n";
	print(begin2);
	if(test(begin1, begin2)==1)
	cout << "\nМножина а є підмножиною б \n";
	else
	cout << "\nМножина а не є підмножиною б\n";
	
	while (begin1)
	{
		list1 = begin1;
		begin1 = list1->next;
		delete list1;
	}
	while (begin2)
	{
		list2 = begin2;
		begin2 = list2->next;
		delete list2;
	}
	cout << endl;
	system("pause");
	return 0;
}

